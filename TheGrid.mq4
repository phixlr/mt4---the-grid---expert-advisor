//+------------------------------------------------------------------+
//|                                               TARS Indicator.mq4 |
//|                                                  Felipe Gerolomo |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Felipe Gerolomo"
#property link "https://www.mql5.com"
#property version "1.00"
//#property indicator_chart_window
//#property indicator_buffers 2

extern int minChannel = 10;
extern int maxChannel = 150;
extern int qtdCandle = 15;
extern int AlertPipRange=1;
extern string AlertWav="alert.wav";

// -- buffers

int currentBars;
int currentChannelCandles;
double currentChannel = 0;
double highCandle;
double lowCandle;
double resistance;
double support;
datetime highCandleTime;
datetime lowCandleTime;
bool isCanal = false;
double tpSL[16];

double topValue;
double bottomValue;
string objectNameTop;
string objectNameBottom;

bool isDrawing = false;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   startTARS();
   return(INIT_SUCCEEDED);
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void startTARS()
  {
   printf("START -->");
   ObjectsDeleteAll();
   datetime startTime = TimeCurrent();
   datetime stopTime = iTime(NULL, PERIOD_D1, 1);

   currentBars = Bars(NULL, 0, startTime, stopTime);

   for(int currentBar=0; currentBar<=currentBars; currentBar++)
     {
      resistance = iFractals(NULL, 0, MODE_UPPER, currentBar);
      support = iFractals(NULL, 0, MODE_LOWER, currentBar);

      if(resistance > 0 && support == 0)
        {
         currentChannel = High[currentBar];
         highCandle = High[currentBar];
         highCandleTime = iTime(0, 0, currentBar);
        }

      if(support > 0 && resistance == 0)
        {

         currentChannel = MathAbs((currentChannel - Low[currentBar]) / _Point);
         currentChannelCandles = Bars(NULL, 0, Time[currentBar], highCandleTime);
         //Print("TAMANHO DO CANAL ATUAL = " + currentChannel + " QUANTIDADE DE CANDLES ATUAL = " + currentChannelCandles);
         if(currentChannel >= minChannel && currentChannel <= maxChannel && isCanal == false)
           {
            isCanal = true;
            isDrawing = true;
            Print("TAMANHO DO CANAL ATUAL = " + currentChannel + " QUANTIDADE DE CANDLES ATUAL = " + currentChannelCandles);

            lowCandleTime = iTime(0, 0, currentBar);
            lowCandle = Low[currentBar];

            drawLine("SUPPORT", lowCandle, C'192,57,43');
            tpSL[0] = lowCandle;

            drawArrow("SUPPORT_ARROW", lowCandleTime, lowCandle, SYMBOL_ARROWUP,  C'192,57,43');
            drawLine("RESISTENCE", highCandle, C'192,57,43');
            tpSL[1] = highCandle;
            drawArrow("RESISTENCE_ARROW", highCandleTime, highCandle+10*Point, SYMBOL_ARROWDOWN,  C'41,128,185');
            drawChannelsTop("TP/SL_TOP_", lowCandle, 7);
            drawChannelsBottom("TP/SL_BOTTOM_", lowCandle, 7);
            ArraySort(tpSL,WHOLE_ARRAY,0,MODE_ASCEND);
            isDrawing = false;
            for(int i = 0; i < ArraySize(tpSL); ++i)
              {
               //Print("TPSL -> " + tpSL[i]);
              }


           }
        }
     }

   verifyTopPosition();
   verifyBottomPosition();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTick()
  {

//printf(currentChannel);

   if(Bid+AlertPipRange*Point >= tpSL[ArraySize(tpSL)-1] && isDrawing == false)
     {
      printf("REDRAW");
      isCanal = false;
      startTARS();
     }


   if(Bid+AlertPipRange*Point >= topValue && OrdersTotal() == 0 && isCanal == true)
     {

      double stoplossBuy = getStopLoss(topValue, "BUY");
      double takeprofitBuy = getTakeProfit(topValue, "BUY");

      int ticketBuy=OrderSend(Symbol(),OP_BUY,1,Ask,3,(stoplossBuy - (50*Point)),takeprofitBuy,"My order",16384,0,clrGreen);

      if(ticketBuy<0)
        {
         Print("OrderSend failed with error #",GetLastError());
        }
      else
        {
         Print("OrderSend placed successfully");
        }


      printf("COMPRA");
     };

   if(Bid+AlertPipRange*Point <= bottomValue && OrdersTotal() == 0 && isCanal == true)
     {
      double stoplossSell = getStopLoss(topValue, "SELL");
      double takeprofitSell = getTakeProfit(topValue, "SELL");

      int ticketSell=OrderSend(Symbol(),OP_SELL,1,Ask,3,(stoplossSell - (50*Point)),takeprofitSell,"My order",16384,0,clrGreen);

      if(ticketSell<0)
        {
         Print("OrderSend failed with error #",GetLastError());
        }
      else
        {
         Print("OrderSend placed successfully");
        }
      printf("VENDE");
     };

   verifyTopPosition();
   verifyBottomPosition();

//return (0);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawLine(string name, double price, color lineColor)
  {
   ObjectCreate(name, OBJ_HLINE, 0, 0, price);
   ObjectSet(name, OBJPROP_STYLE, STYLE_SOLID);
   ObjectSet(name, OBJPROP_COLOR, lineColor);
   ObjectSet(name, OBJPROP_WIDTH, 2);
   return;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawArrow(string name, datetime time, double price, int arrowType, color lineColor)
  {
   ObjectCreate(name, OBJ_ARROW, 0, time, price); //draw an up arrow
   ObjectSet(name, OBJPROP_STYLE, STYLE_SOLID);
   ObjectSet(name, OBJPROP_ARROWCODE, arrowType);
   ObjectSet(name, OBJPROP_COLOR, lineColor);
   ObjectSet(name, OBJPROP_WIDTH, 5);
   return;
  }

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void drawChannelsTop(string name, double price, int qtdChannel)
  {
   price = price + (currentChannel * Point);
   for(int y = 0; y < qtdChannel; y++)
     {
      price = price + (currentChannel * Point);
      drawLine(name + y, price, C'39,174,96');
      tpSL[y+2] = price;
     }
   return;
  }
//+------------------------------------------------------------------+
void drawChannelsBottom(string name, double price, int qtdChannel)
  {
   for(int y = 0; y < qtdChannel; y++)
     {
      price = price - (currentChannel * Point);
      drawLine(name + y, price, C'39,174,96');
      tpSL[y+9] = price;
     }
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double findInArray(double value)
  {
   for(int i = 0; i < ArraySize(tpSL); ++i)
     {
      printf(tpSL[i]);
      if(tpSL[i] == value)
        {
         printf("<--ACHOU-->" + value);
         return tpSL[i];
        }
     }

   return 0;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void verifyTopPosition()
  {
   for(int a = ArraySize(tpSL); a > 0; --a)
     {
      if(Bid < tpSL[a])
        {
         topValue = tpSL[a];
        }
     }

   for(int iObj = ObjectsTotal() - 1; iObj >= 0; iObj--)
     {
      string nameObj = ObjectName(iObj);
      if(ObjectType(nameObj) == OBJ_HLINE)
        {
         double lineValue = ObjectGet(nameObj, OBJPROP_PRICE1);
         if(lineValue == topValue)
           {
            objectNameTop = nameObj;
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void verifyBottomPosition()
  {
   for(int i = 0; i < ArraySize(tpSL); ++i)
     {
      if(Bid > tpSL[i])
        {
         bottomValue = tpSL[i];
        }
     }

   for(int iObj = ObjectsTotal() - 1; iObj >= 0; iObj--)
     {
      string nameObj = ObjectName(iObj);
      if(ObjectType(nameObj) == OBJ_HLINE)
        {
         double lineValue = ObjectGet(nameObj, OBJPROP_PRICE1);
         if(lineValue == bottomValue)
           {
            objectNameBottom = nameObj;
           }
        }
     }
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getStopLoss(double value, string typeOrder)
  {
   for(int i = 0; i < ArraySize(tpSL); ++i)
     {
      if(value == tpSL[i] && typeOrder == "BUY")
        {
         return (tpSL[i-2]);
        }

      if(value == tpSL[i] && typeOrder == "SELL")
        {
         return (tpSL[i+2]);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getTakeProfit(double value, string typeOrder)
  {
   for(int i = 0; i < ArraySize(tpSL); ++i)
     {
      if(value == tpSL[i] && typeOrder == "BUY")
        {
         return (tpSL[i+2]);
        }

      if(value == tpSL[i] && typeOrder == "SELL")
        {
         return (tpSL[i-2]);
        }
     }
  }
//+------------------------------------------------------------------+
